# -*- coding: utf-8 -*-
from sensor import *
import numpy
from datetime import datetime
from flask import Flask, abort, jsonify
from sensor import *
from data_process2 import *
import json
import pandas as pd
import datetime
import time
from sklearn.externals import joblib

reg = joblib.load('reg.pkl') 
app = Flask(__name__)

Ohyama = Ohyama()
columns_df = ['time',
           'numerics.temperature.atmospheric',
           'numerics.humidity.atmospheric',
           'numerics.illumination.atmospheric',
           'numerics.rain.precipitation',
           'numerics.wind.direction',
           'numerics.wind.velocity',
           'numerics.wind.maxInstantaneousVelocity',
           'numerics.pressure.atmospheric']


@app.route('/get_weather/')
def get_weather():
    data = Ohyama.get_data_now3()
    if len(data) != 7:
        return [0.0, 0.0, 0.0, 0.0]
    df = pd.DataFrame(data, columns = columns_df)
    df = df2float(df)
    df = v2sincos(df)
    df = calc_diff(df)
    row = []
    if check_rain(df):
        row += [0.0, 1.0]
    else:
        row += [1.0, 0.0]
    row += get_rain(df)  # 2
    row += get_hum(df)   # 3
    row += get_ill(df)   # 4
    row += get_dir(df)   # 6
    row += get_diff(df)  # 6
    row += get_velm(df)  # 7   total 30

    time = datetime.datetime.now().isoformat()
    result = reg.predict(row)[0]
    jsonString = '''
        { "time" :  "%s" ,
          "sun2sun"  : %s ,
          "sun2rain" : %s ,
          "rain2sun" : %s ,
          "rain2rain": %s 
        }''' % (time, result[0], result[1], result[2], result[3])
    return jsonify(json.loads(jsonString)), {'Access-Control-Allow-Origin': '*'}
    

if __name__ == '__main__':
    app.run(host='0.0.0.0')
    
    
