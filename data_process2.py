# -*- coding: utf-8 -*-
import numpy as np
import csv
import pandas as pd
import datetime
import math
import time


columns = ['now_sunny',
           'now_rain',
           'count_rain',
           'count_freq',
           'hum_80',
           'hum_80_92',
           'hum_92_',
           'ill_0',
           'ill_0_1000',
           'ill_1000_15000',
           'ill_15000_',
           'dir_1',
           'dir_2',
           'dir_3',
           'dir_4',
           'dir_5',
           'dir_6',
           'diff_1',
           'diff_2',
           'diff_3',
           'diff_4',
           'diff_5',
           'diff_6',
           'velm_1',
           'velm_2',
           'velm_3',
           'velm_4',
           'velm_5',
           'velm_6',
           'velm_7'
          ]

def df2float(df):
    d = df.as_matrix().astype(np.float64)
    datetime_list = []
    for t in d.T[0]:
        datetime_list.append(pd.to_datetime(datetime.datetime(*time.localtime(float(t)/1000)[:6])))
    break_list = [1]
    diff_cos = [0.0]
    diff_cos = [0.0]
    for i in range(1,len(d.T[0])):
        if d.T[0][i] - d.T[0][i-1]  > 1000000.0:
            break_list.append(1)
        else:
            break_list.append(0)
    
    df_tmp = pd.DataFrame(d, columns=df.columns)
    df_tmp['detetime'] = datetime_list
    df_tmp['break'] = break_list
    
    return df_tmp

def v2sincos(df):
    df_tmp = df
    v_list = df[['numerics.wind.direction']].as_matrix().T[0]
    result = np.array([np.cos(np.deg2rad(v_list))])
    result = np.append(result, np.array([np.sin(np.deg2rad(v_list))]), axis=0)
    #print(result[0])
    df_tmp['wind_direction_cos'] = result[0]
    df_tmp['wind_direction_sin'] = result[1]
    return df_tmp

def calc_diff(df):
    diff_wind_direction = [0.0]
    matrix = df[['break', 'numerics.wind.direction']].as_matrix()
    for i in range(1,len(matrix)):
        if matrix[i][0] == 1:
            diff_wind_direction.append(0.0)
        else:
            if matrix[i][1] >= matrix[i-1][1]:
                if matrix[i][1] - matrix[i-1][1] <= 180:
                    diff_wind_direction.append(matrix[i][1] - matrix[i-1][1])
                else:
                    diff_wind_direction.append( -360.0 + matrix[i][1] - matrix[i-1][1])
            else:
                if matrix[i-1][1] - matrix[i][1] <= 180:
                    diff_wind_direction.append(- matrix[i-1][1] + matrix[i][1])
                else:
                    diff_wind_direction.append(360.0 - (matrix[i-1][1] - matrix[i][1]))
                    
    df_tmp = df
    df_tmp['diff_wind_direction'] = diff_wind_direction
    return df_tmp

def check_rain(df):
    if len(df[['numerics.rain.precipitation']].as_matrix()) != 7:
        return -1
    if df[['numerics.rain.precipitation']].as_matrix()[-1][0] == 0.0:
        return 0
    else:
        return 1

def get_rain(df):
    count = -1.0
    count_p = 0.0
    bo = True
    for r in reversed(list(df[['numerics.rain.precipitation']].as_matrix().T[0])):
        if r > 0:
            count += 1.0
            if bo == False:
                count_p += 1
                bo = False
        else:
            if bo == True:
                count_p += 1
                bo = True
        
    return [count/6.0 , count_p/6.0]

def get_hum(df): 
    hum = df[['numerics.humidity.atmospheric']].as_matrix().T[0][-1]
    result = [0.0, 0.0, 0.0]
    if hum < 80.0:
        result[0] = 1.0
    elif hum < 92.0:
        result[1] = 1.0
    else:
        result[2] = 1.0
    return result


def get_ill(df): 
    ill = df[['numerics.illumination.atmospheric']].as_matrix().T[0][-1]
    result = [0.0, 0.0, 0.0, 0.0]
    if ill == 0.0:
        result[0] = 1.0
    elif ill < 1000:
        result[1] = 1.0
    elif ill < 15000:
        result[2] = 1.0
    else:
        result[3] = 1.0
    return result

def get_dir(df):# 風の向きのコサイン類似度　６
    rain_cos_sin = np.load('cluster_data/rain_cos_sin.npy')
    d_cs = df[['wind_direction_cos', 'wind_direction_sin']].as_matrix()[-1]
    min_len = 100.0
    max_i = 0
    for i,cs in enumerate(rain_cos_sin):
        if np.sqrt(np.power(d_cs - cs, 2).sum()) < min_len:
            min_len = np.sqrt(np.power(d_cs - cs, 2).sum())
            max_i = i
    
    
    result = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    result[max_i] = 1.0
    return result

def get_diff(df): # 風の向きの変化量のコサイン類似度　６
    diff_wind_direction = np.load('cluster_data/diff_wind_direction.npy')
    d_diff = df[['diff_wind_direction']].as_matrix().T[0] / 180.0
    min_len = 100.0
    max_i = 0
    for i,diff in enumerate(diff_wind_direction):
        if np.sqrt(np.power(diff - d_diff, 2).sum()) < min_len:
            min_len = np.sqrt(np.power(diff - d_diff, 2).sum())
            max_i = i
            
    result = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    result[max_i] = 1.0
    return result

def get_velm(df):
    velm_data = np.load('cluster_data/velm_wind.npy')
    velm = df[['numerics.wind.maxInstantaneousVelocity']].as_matrix().T[0]
    for i, vm in enumerate(velm):
        if vm < 1.1:
            velm[i] = 0.0
        elif vm < 2.2:
            velm[i] = 1.0
        else:
            velm[i] = 2.0
    
    min_len = 100.0
    max_i = 0
    for i,vm in enumerate(velm_data):
        if np.sqrt(np.power(velm - vm, 2).sum()) < min_len:
            min_len = np.sqrt(np.power(velm - vm, 2).sum())
            max_i = i
    
    result = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    result[max_i] = 1.0
    return result


        
