FROM python:2.7
LABEL src-repo="https://bitbucket.org/petadata/weather_forecast2”
EXPOSE 5000
RUN pip install --upgrade setuptools
RUN pip install --upgrade pip
RUN pip install pandas numpy scipy Flask httplib2 sklearn
WORKDIR /app
COPY . /app
CMD python est_REST2.py
