# 概要
## 

# イメージを作成する方法
```
git clone https://bitbucket.org/petadata/weather_forecast2.git
cd weather_forecast2
docker build -t weather_forecast:latest .
```

```
ryoubokushunsaenoMacBook-Pro:docker hariki$ docker images
REPOSITORY                     TAG                 IMAGE ID            CREATED             SIZE
weather_forecast               latest              158153d9bf0c        10 minutes ago      1.16 GB
```


# 起動方法
```
docker run -it --name weather_forecast -p 5000:5000 weather_forecast
```

# APIのresponse
## /get_weather/
現在から20分後の天気を予測
```
{
  "rain2rain": 0.0, 
  "rain2sun": 0.0, 
  "sun2rain": 0.0, 
  "sun2sun": 1.0, 
  "time": "2017-01-27T06:51:05.109223"
}
```