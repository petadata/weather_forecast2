import urllib2
import re
import numpy as np
from datetime import datetime
import time

API_KEY = '568924219A5165A0908BF285F736CB60C3B3668BE85065506E8612D6C29ED087'
URL = 'http://idea.allegrosmart.com/api/dp/v3.7/formeddata?contractorNo=idea&apikey='+API_KEY+'&'

class Sensor:
    def __init__(self,name='', userNo='', assetNo=1, metrics=[], raw= True,
                 form='csv', showTimeStr=False, cate_URL='', rountMs=False):
                 #From=False, To=False, interval=False):
        self.name = name
        self.userNo = userNo
        self.assetNo = assetNo
        self.metrics = metrics
        self.raw = raw
        self.form = form
        self.showTimeStr = showTimeStr
        self.cate_URL = cate_URL
        self.rountMs = rountMs
        #self.From = From
        #self.To = To
        #self.interval = interval
        self.url = self.create_URL()
    
    def create_URL(self):
        url = URL + 'userNo=' + self.userNo + '&assetNo=' + str(self.assetNo) + '&'
        for m in self.metrics:
            url += 'metrics=' + m + '&'
        if self.raw == True:
            url += 'raw=true&'
        else:
            url += 'raw=false&'
        url += 'format=' + self.form + '&'
        if self.showTimeStr == True:
            url += 'showTimeStr=true&'
        else:
            url += 'showTimeStr=false&'
        #if self.From != False:
            #url += 'from=' + self.From + '&'
        #if self.To != False:
            #url += 'To=' + self.To + '&'
        #if self.interval != False:
            #url += 'interval=' + self.interval + '&'
        if self.rountMs == True:
            url += 'rountMs=true&'
        if self.showTimeStr == True:
            url += 'showTimeStr=true'
        else:
            url += 'showTimeStr=false'
        return (url)

    def get_data(self, From=False, To=False, interval=False):
        url = self.url
        if From != False or To != False or interval != False:
            url += '&'
                    
        if From != False:
            url += 'from=' + From + '&'
        if To != False:
            url += 'To=' + To + '&'
        if interval != False:
            url += 'interval=' + interval + '&'
        url = url[:-1]
        response = urllib2.urlopen(url)
        html = response.read()
        data = html.split("\n")
        result = []
        num_reg = re.compile('^[+-]?(\d*\.\d+|\d+\.?\d*)([eE][+-]?\d+|)\Z')
        label = data[0].split(",")
        for d in data[1:]:
            a = d.split(",")
            for i in range(len(a)):
                if bool(num_reg.match(a[i])):
                    a[i] = float(a[i])
            result.append(a)
        result = result[:-1]
        return ([label, result])

class Ohyama:
    URL = 'http://idea.allegrosmart.com/api/dp/v3.7/formeddata?contractorNo=idea&apikey='+API_KEY+'&'
    def __init__(self):
        self.sensor_list = self.get_sensor_list()
        

    def get_sensor_list(self):
        sl = []
        sl.append(Sensor(name='oym_dt_highst', 
                         userNo='u3500553',
                         assetNo=1,
                         cate_URL=URL,
                         metrics=['numerics.temperature.atmospheric',
                                  'numerics.humidity.atmospheric',
                                  #'numerics.illumination.ultraviolet"',
                                  'numerics.illumination.atmospheric',
                                  'numerics.rain.precipitation',
                                  'numerics.wind.direction',
                                  'numerics.wind.velocity',
                                  'numerics.wind.maxInstantaneousVelocity',
                                  'numerics.pressure.atmospheric']))
        
        #sl.append(Sensor(name='oym_dt_mrd', 
                         #userNo='u3500552',
                         #assetNo=1,
                         #cate_URL=URL,
                         #metrics=['numerics.temperature.atmospheric',
                                  #'numerics.humidity.atmospheric',
                                  #'numerics.illumination.ultraviolet"',
                                  #'numerics.illumination.atmospheric',
                                  #'numerics.rain.precipitation',
                                  #'numerics.wind.direction',
                                  #'numerics.wind.velocity',
                                  #'numerics.wind.maxInstantaneousVelocity',
                                  #'numerics.pressure.atmospheric']))

        sl.append(Sensor(name='oym_dt_tkd', 
                         userNo='u3500551',
                         assetNo=1,
                         cate_URL=URL,
                         metrics=['numerics.temperature.atmospheric',
                                  'numerics.humidity.atmospheric',
                                  #'numerics.illumination.ultraviolet',
                                  'numerics.illumination.atmospheric',
                                  'numerics.rain.precipitation',
                                  'numerics.wind.direction',
                                  'numerics.wind.velocity',
                                  'numerics.wind.maxInstantaneousVelocity',
                                  'numerics.pressure.atmospheric']))
        return(sl)
    
    def get_data(self, From=False, To=False, interval=False):
        result = []
        for s in self.sensor_list:
            result.append(s.get_data(From=From, To=To, interval=interval))
        #self.data = result
        return(result)

    def get_data_arranged(self, From=False, To=False, interval=False):
        result = []
        label = []
        tmp = []
        count = 0
        for s in self.sensor_list:
            tmp.append(s.get_data(From=From, To=To, interval=interval))
            count+=1
        j_point = 0
        for i in range(len(tmp[0][1])):
            time_0 = tmp[0][1][i][0]
            deff_time = 1000000
            for j in range(j_point,len(tmp[1][1])):
                deff_time = time_0 - tmp[1][1][j][0]
                if deff_time <= 0:
                    result.append(tmp[0][1][i] + tmp[1][1][j][1:])
                    break;
        label = tmp[0][0] + tmp[1][0][1:]
        return([label, result])

    def get_data_now(self):
        now = datetime.now()
        result = [[],[]]
        f = ''
        #if now.hour < 2:
            #f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % now.day + 'T22:00:00%2B00:00'
        #else:
            #f = str(now.year) + str(now.month) + '-' + str(now.day) + 'T' + str(now.hour-1) + ':00:00%2B00:00'
        f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % (now.day-1) + 'T00:00:00%2B00:00'
        s1 = self.sensor_list[0].get_data(From = f, To = False, interval = '600000')[1][-6:]
        s2 = self.sensor_list[1].get_data(From = f, To = False, interval = '60000')[1][-60:]
        return [s1,s2]

    def get_data_now2(self):
        now = datetime.now()
        result = [[],[]]
        f = ''
        #if now.hour < 2:
            #f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % now.day + 'T22:00:00%2B00:00'
        #else:
            #f = str(now.year) + str(now.month) + '-' + str(now.day) + 'T' + str(now.hour-1) + ':00:00%2B00:00'
        f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % (now.day-1) + 'T00:00:00%2B00:00'
        #self.sensor_list[0].raw = False
        #self.sensor_list[1].raw = False
        s1 = self.sensor_list[0].get_data(From = f, To = False, interval = '600000')[1]#[-6:]
        s2 = self.sensor_list[1].get_data(From = f, To = False, interval = '60000')[1]#[-60:]
        s1_count = -1
        s1_list = []
        while True:
            boolean = True
            for s in s1[s1_count]:
                if s == '':
                    boolean = False
            if boolean:
                s1_list.append(s1[s1_count])
            if len(s1_list) == 6:
                break
            else:
                s1_count -= 1
        
        s2_count = -1
        s2_list = []
        while True:
            boolean = True
            for s in s2[s2_count]:
                if s == '':
                    boolean = False
            if boolean:
                s2_list.append(s2[s2_count])
            if len(s2_list) == 60:
                break
            else:
                s2_count -= 1
        
        
        label = [0.0,0.0,0.0,0.0]
        if s1[-1][4] == 0:
            label[0] = 1.0
        else:
            label[2] = 1.0
        return [s1_list,s2_list,np.array(label)]

    def get_data_now3(self):
        now = datetime.now()
        result = [[],[]]
        f = ''
        #if now.hour < 2:
            #f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % now.day + 'T22:00:00%2B00:00'
        #else:
            #f = str(now.year) + str(now.month) + '-' + str(now.day) + 'T' + str(now.hour-1) + ':00:00%2B00:00'
        f = '%04d' % now.year + '-' + '%02d' % now.month + '-' + '%02d' % (now.day-1) + 'T00:00:00%2B00:00'
        s1 = self.sensor_list[0].get_data(From = f, To = False, interval = '600000')[1][-7:]
        return s1


    

    
    
    
    

#def main():
    #s1 = Sensor(assetNo = 1, metrics=['numerics.temperature.atmospheric'])
    #label, data = s1.get_data()
    #ohyama = Ohyama()
    #ohyama.get_data_arranged()
    #print(ohyama.data)
    #print(data)


#http://idea.allegrosmart.com/api/dp/v3.7/formeddata?contractorNo=idea&apikey=568924219A5165A0908BF285F736CB60C3B3668BE85065506E8612D6C29ED087&userNo=u3500551&assetNo=1&metrics=numerics.temperature.atmospheric&metrics=numerics.humidity.atmospheric&metrics=numerics.pressure.atmospheric&metrics=numerics.illumination.atmospheric&metrics=numerics.wind.direction&metrics=numerics.wind.velocity&metrics=numerics.rain.precipitation&raw=true&format=csv&showTimeStr=true


#if __name__ == '__main__':
    #main()
